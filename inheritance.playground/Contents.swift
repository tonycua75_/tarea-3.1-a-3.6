//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"


class animal{
    var name = "Default"
    var age = 0
    
    func getDetails() -> String {
    return "this animal name is \(name). It is \(age) year old."
    }
    
}

class dog : animal{
    
    func woof() -> String{
        return "woof woof woof"
    }
    
}

class cat : animal{
    
    func miau() -> String{
        return "miau miau miau"
    }
    
}


var RayansDog = dog()
RayansDog.name = "Toto"
RayansDog.age = 16
RayansDog.woof()
RayansDog.getDetails()

var Rayancat = cat()
Rayancat.name = "Hugo"
Rayancat.age = 11
Rayancat.miau()
Rayancat.getDetails()

