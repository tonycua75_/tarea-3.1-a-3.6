//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"


class animal {
    
    var name = "default"
    var age = 0
    
    func getDetails() -> String {
        return "this animals name is \(name). It is \(age) years old"
    }
    
}

var miAnimal = animal()
miAnimal.name = "peter"
miAnimal.age = 2
miAnimal.getDetails()




